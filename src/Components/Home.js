import React,{useContext} from 'react'
import {Odoo} from '../ContextApi'

export default function Home() {
    const {name} = useContext(Odoo)
    return (
        <div>
            {name}
        </div>
    )
}
