import React, { Component,createContext } from 'react'


export const Odoo = createContext()
export class OdooProvider extends Component {
    state={
        name:"Odoo"
    }
    render() {
        return (
            <Odoo.Provider value={{
                ...this.state
            }}>
                {this.props.children}
            </Odoo.Provider>
        )
    }
}
